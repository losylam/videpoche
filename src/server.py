#!/env/python
# coding: utf-8

from flask import Flask, render_template, request, jsonify, redirect, url_for
import json

app = Flask(__name__)

def create_csv(filename, data):
    txt = ''
    for c, i in data.items():
        txt += '%s; %s; %s\n'%(c, i['name'], i['n'])

    with open(filename, 'w') as f:
        f.write(txt.encode('utf-8'))

def get_ressources_csv(filename):
    data = {}
    with open(filename, "r") as f:
        for i in f.readlines():
            iso_a3, name, n = i.split(';')
            data[iso_a3] = {'name':name, 'code':iso_a3, 'n':n.strip()}
    return data
        
def get_ressources(filename):
    with open(filename, "r") as f:
        data = json.load(f)
    return data

data_csv = get_ressources_csv('data.csv')
data = get_ressources('data.json')

for c, i in data_csv.items():
    data[c]['n'] = i['n']

@app.route('/')
def index():
    return render_template('index.html', toapropos = False)

@app.route('/apropos')
def apropos():
    return render_template('index.html', toapropos = True)

@app.route('/results', methods=['GET', 'POST'])
def results():
    json.dump(data, open('data.json', 'w'), indent = 2)    
#    create_csv('data.csv', data)
    return render_template('results.html', data = data)

@app.route('/postmethod', methods = ['POST'])
def get_post_javascript_data():
    jsdata = request.form['javascript_data']
    jj = json.loads(jsdata)
    for i in jj:
        data[i['id']]['n'] =  int(data[i['id']]['n']) + int(i['last'])
    create_csv('data.csv', data)
    json.dump(data, open('data.json', 'w'), indent = 2)
    return jsdata
    
@app.route("/data")
def get_data():
    return jsonify(data)

@app.route("/raz")
def raz():
    for c, i in data.items():
        i['n'] = 0
    create_csv("data.csv", data)
    return "Les données ont bien été remises à zéro."

    
if __name__ == '__main__':
    app.run(debug=False)
    

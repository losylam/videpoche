$(document).ready(function(){
    $('.vp-quantity').each(function() {
	var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.vp-quantity-up'),
            btnDown = spinner.find('.vp-quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');
	
	btnUp.click(function() {
	    console.log('click up');
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
		var newVal = oldValue;
            } else {
		var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
	});
	
	btnDown.click(function() {
	    console.log('click down');
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
		var newVal = oldValue;
            } else {
		var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
	});
	
    });

    $('.vp-flag').each(function(){
	var flag = $(this),
	    w = flag.find('.vp-flag-img').width();

	console.log(w);
    });
    
});

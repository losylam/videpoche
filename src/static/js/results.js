var data_coin;
var regions;
var path;
var carto;
var features;
var map_data;
var simulation;
var sim;

$(document).ready(function(){

    var width = 1920;
    var height = 1075;

    var bar_w = 900;
    var bar_h = 900;

    var FRA_pts = [550, 610];
//    var carto;
//    var path;
//    var features;

    var update;
    var voix;
    var names;
    var next_step;

    var fl_grid;
    var flags_c;
    
    var color_min = '#fff' ;
    var color_max = '#003399';

    var lst_step = [];
    var i_step = 1;

    // d3.json('data', function(data){
    // 	console.log('ok json');
    // 	console.log(data);
    // });

    var links = [];
    
    data_coin = [ {'iso_a3':'FRA', 'h':160, 'w':240, 'name':"France"},
		  {'iso_a3':'DEU', 'h':160, 'w':267, 'name':"Allemagne"}, 
		  {'iso_a3':'AUT', 'h':160, 'w':240, 'name':"Autriche"},
		  {'iso_a3':'BEL', 'h':160, 'w':240, 'name':"Belgique"},
		  {'iso_a3':'CYP', 'h':160, 'w':267, 'name':"Chypre"},
		  {'iso_a3':'ESP', 'h':160, 'w':240, 'name':"Espagne"},
		  {'iso_a3':'EST', 'h':160, 'w':251, 'name':"Estonie"},
		  {'iso_a3':'FIN', 'h':160, 'w':262, 'name':"Finlande"}, 
		  {'iso_a3':'GRC', 'h':160, 'w':240, 'name':"Grèce"},
		  {'iso_a3':'IRL', 'h':160, 'w':320, 'name':"Irlande"},
		  {'iso_a3':'ITA', 'h':160, 'w':240, 'name':"Italie"}, 
		  {'iso_a3':'LVA', 'h':160, 'w':320, 'name':"Lettonie"},
		  {'iso_a3':'LTU', 'h':160, 'w':267, 'name':"Lituanie"},
		  {'iso_a3':'LUX', 'h':160, 'w':267, 'name':"Luxembourg"}, 		 
		  {'iso_a3':'MLT', 'h':160, 'w':240, 'name':"Malte"}, 
		  {'iso_a3':'NLD', 'h':160, 'w':240, 'name':"Pays-Bas"},
		  {'iso_a3':'PRT', 'h':160, 'w':240, 'name':"Portugal"}, 
		  {'iso_a3':'SVK', 'h':160, 'w':240, 'name':"Slovaquie"}, 
		  {'iso_a3':'SVN', 'h':160, 'w':320, 'name':"Slovénie"}]

    var lst_iso = data_coin.map(function(d){return d.iso_a3});

    map_data = Object();

    var data_coin_last = [];
    var prop_c = 0.7;
    var m_top = 42;
    
    var but_h = 50;
    var m_lat = 10;
    var m_bot = but_h + 2*m_lat;

    var h_c_tot = 160 * prop_c + m_top + m_bot;
    
    var marg = 25;
    var pos_y = 60; // y init = marge en haut
    var pos_x = 0;

    var step_y = h_c_tot + marg;

    var flag_g_l = 2*width/3;
    
    data_coin.forEach(function(d){
	var dat = d3.select('.vp-data-'+d['iso_a3']);
	d['id'] = d['iso_a3'];
	d['last'] = 0;
	d['tot'] = parseInt(dat.select('.vp-data-tot').text());
	d['prop'] = d['w']/d['h'];
	d['h_c'] = d['h']*prop_c + m_top + m_bot;
	d['w_c'] = d['w']*prop_c + 2*m_lat;

	pos_x = pos_x + d['w_c'] + marg;
	
	if (pos_x + marg > flag_g_l){
	    pos_x = marg + d['w_c'];
	    pos_y += step_y
	}
	d['pos_x'] = pos_x - d['w_c'];
	d['pos_y'] = pos_y;
	console.log(d['tot']);
	map_data[d.iso_a3] = {'d':d, 'tot':d['tot'], 'last':d['last']};
	data_coin_last.push(d);
    });
    
    var max_coin = d3.max(data_coin.map(function(d){return d.tot}));
    var max_coin_fr = d3.max(data_coin.filter(function(d){return d.iso_a3 != "FRA";})					      
			     .map(function(d){return d.tot}));

    data_coin_last.sort(function(a, b){return d3.descending(a.last, b.last);});
    data_coin.sort(function(a, b){return d3.descending(a.tot, b.tot);});

    var svg = d3.select('#container')
	.append("svg")
	.attr("width", width)
    	.attr("height", height);


    $.idleTimer(60000);
    
    $(document).on( "idle.idleTimer", function(event){
    	window.location.href = 'http://'+window.location.host;
    });
	 
    
    var draw = function(error, data){
	if (error) return console.error(error);
	
	var topology = data;
	var geometries = topology.objects.collection;
	
	projection = d3.geoMercator()
 	    .scale(1100)
 	    .translate([500, 1650]);
	
	path = d3.geoPath()
	    .projection(projection);
	
	regions = svg.append('g')
	    .attr("id", "regions");
//	    .selectAll("path");
	
	carto = d3.geoPath()
	    .projection(projection);
	    // .properties(function(d) {
	    // 	var a = Object();
	    // 	a['iso_a3'] = d.properties.iso_a3;
	    // 	a['val'] = 0.0;
		
	    // 	return a;
	    // });
	
	features = topojson.feature(topology,geometries).features;

	regions = regions.selectAll('path')
	    .data(features)
	    .enter()
	    .append("path")
	    .attr("class", "region")
	    .style("opacity", "0.5")
	    .attr("d", path);
	
	regions.data(features)
	    .transition()
	    .duration(750)
	    .style("fill", "#fff")
	    .style("stroke", "#000");

	var centroid = [];
	features.forEach(function(d){
	    var ii = data_coin.find(function(j){return j.iso_a3 == d.properties.iso_a3})

	    if (ii){
		ii['centroid'] = path.centroid(d);
	    }
	    if (d.properties.iso_a3 == "FRA"){
		ii['centroid'] = FRA_pts;
	    }else if (d.properties.iso_a3 == "FIN"){
		ii['centroid'] = [1000, 150];
	    }
	});

	init_flags();

	var get_results = function(){
	    $.post("/postmethod", {
		javascript_data: JSON.stringify(data_coin)
	    })
	    
	    results();
	    sim();
	}

	d3.select('#vp-infos')
	    .append('div')
	    .attr('class', 'vp-valider vp-button')
	    .style('top', '800px')
	    .on('click', get_results)
	    .append('div')
	    .attr('class', 'vp-valider-txt')
	
	    .text('Valider');
	
	d3.select('.vp-button-accueil')
	    .on('click', function(){
		
		setTimeout(function(){
		    window.location.href = 'http://'+window.location.host;
		}, 100);
		// retourner à l'accueil, envoyer les données post
	    })
    }
    
    d3.json("static/data/europe.topojson", draw);

    var init_flags = function(){


	diag_links = svg.append("g")
	    .attr("class", "diag_links");
	

	fl_grid = svg.append("g")
	    .attr("class", "vp-flags");
	
	fl_grid.selectAll('g')
	    .data(data_coin)
	    .enter()
	    .append('g')
	    .attr("class", 'flag')
	    .attr("transform", function(d){
		return "translate(" + d.pos_x + ", " + d.pos_y + ")";
	    })
	    .append("rect")
	    .style("fill", "#fff")
	    .style("opacity", "0.8")
	    .attr("x", 0)
	    .attr("y", 0)
	    .attr("height", function(d){
		return d.h_c;
	    })
    	    .attr("width", function(d){
		return d.w_c;
	    });

	// nom des pays
	fl_grid.selectAll('g .flag')
	    .append('text')
	    .attr('class', 'flag-title')
	    .attr('text-anchor', 'middle')
	    .attr('transform', function(d){
		return "translate(" + d.w_c/2 + ", 30)";
	    })
	    .text(function(d){
		return d.name;
	    });
	
	// nombre de pièces
	fl_grid.selectAll('g .flag')
	    .append('text')
	    .attr('id', function(d){
		return 'flag-nb-'+d.id
	    })
	    .attr('class', 'flag-nb')
	    .attr('text-anchor', 'middle')
	    .attr('transform', function(d){
		return "translate(" + d.w_c/2 + ", " + (d.h_c - 20) +")";
	    })
	    .text(function(d){
		return d.last;
	    });
	
	// bouton moins
	fl_grid.selectAll('g .flag')
	    .append('g')
	    .attr('class', 'flag-button')
	    .on('click', function(d){
		d.last = d3.max([d.last - 1, 0]);
		d3.select('#flag-nb-'+ d.id)
		    .text(d.last);
	    })
	    .append('rect')
	    .attr('class', 'flag-button moins')
	    .attr('x', m_lat)
	    .attr('y', function(d){
		return d.h_c - m_bot + m_lat;
	    })
	    .attr('width', but_h)
	    .attr('height', but_h)
	    .style('fill', '#ddd');
	
	fl_grid.selectAll('g .flag-button')
	    .append('text')
	    .attr('class', 'flag-signe moins')
	    .attr('text-anchor', 'middle')
	    .attr('transform', function(d){
		var x = m_lat + but_h/2;
		var y = d.h_c - 23;
	     	return "translate(" + x + ", " + y +")";
	    })
	    .text(function(d){
	     	return "-";
	    });
	
	// bouton plus
	fl_grid.selectAll('g .flag')
	    .append('g')
	    .attr('class', 'flag-button')
	    .on('click', function(d){
		d.last = d3.min([d.last + 1, 9]);
		d3.select('#flag-nb-'+ d.id)
		    .text(d.last);
	    })
	    .append('rect')
	    .attr('class', 'flag-button plus')
	    .attr('x', function(d){
		return d.w_c - m_lat - but_h;
	    })
	    .attr('y', function(d){
		return d.h_c - m_bot + m_lat;
	    })
	    .attr('width', but_h)
	    .attr('height', but_h)
	    .style('fill', '#ddd');

	
	fl_grid.selectAll('g .flag-button')
	    .append('text')
	    .attr('class', 'flag-signe plus')
	    .attr('text-anchor', 'middle')
	    .attr('transform', function(d){
		var x = d.w_c - m_lat - but_h/2;
		var y = d.h_c - 23;
	     	return "translate(" + x + ", " + y +")";
	    })
	    .text("+");
		  
	flags_c = svg.append("g")
	    .attr("class", "flags-container");
	    //.attr("transform", "translate(200, 100)");
	
	flags_c.selectAll("g .ticks")
	    .data(data_coin)
	    .enter()
	    .append('g')
	    .attr('class', 'ticks')
   	    .attr("transform", function(d){
		var xx = d.pos_x + m_lat;
		var yy = d.pos_y + m_top;
		return "translate(" + xx + ", " + yy + ")"
	    })
   	    .append("svg:image")
    	    .attr("xlink:href", function(d){return 'static/imgs/thumb/'+d.iso_a3+'.png'})
    	    .attr("height", function(d){
		return d.h*prop_c;
	    })
    	    .attr("width", function(d){
		return d.w*prop_c;
	    });

	// fl.selectAll('.flag')
	//     .append("image")
    	//     .attr("xlink:href", function(d){return 'static/imgs/thumb/'+d.iso_a3+'.png'})
    	//     .attr("height", function(d){
	// 	return d.h*0.9;
	//     })
    	//     .attr("width", function(d){
	// 	return d.w*0.9;
	//     });

    }
    sim = function(){
	var sq = d3.scaleSqrt().range([0, 300]).domain([0, max_coin]);
	
	var link_force = d3.forceLink()
	    .id(function(d){return d.id;})
	    .distance(function(d) {
		return (sq(d.source.tot) +sq(d.target.tot)) /2
	    })
	    .strength(-0.1);
	
	var collisionForce = rectCollide().size(function(d){
	    return [d.ww, d.hh]
	})
	    .iterations(12);
	
	simulation = d3.forceSimulation()
	    .force("link", link_force)
	//		.force("charge", d3.forceManyBody())
	//		.force("center", d3.forceCenter(width/2, height/2))
	    .force("collision", collisionForce) 
	    .force('x', d3.forceX(function (d) {
		return d.centroid[0];
	    }).strength(1))
	    .force('y', d3.forceY(function (d) {
		return d.centroid[1];
	    }).strength(1));
	
	var link = svg.append("g")
	    .attr("class", "links")
	    .selectAll("line")
	    .data(links)
	    .enter()
	    .append("line")
	    .style("stroke", "#000")
	    .style("stroke-width", 0.1);
	
	
	var node = svg.append("g")
	    .attr("class", "nodes")
	    .selectAll("rect")
	    .data(data_coin)
	    .enter()
	    .append("g")
	    .attr("class", "node")
	    .append("rect")
	    .attr("width", function(d){
		d.ww = sq(d.tot)
		return d.ww;
	    })
	    .style("opacity", 0.0) // debug
	    .style("fill", "#a00")
	    .style("stroke", "#000")
	    .attr("height", function(d){
		d.hh = sq(d.tot)/d.prop
		return d.hh;
	    })
	    .attr("x", function(d){
		return d['centroid'][0];
	    })
	    .attr("y", function(d){
		return d['centroid'][1];
	    })
	    .attr("transform", function(d){
		return "translate(-" + d.ww/2 + ', -' + d.hh/2+')'; 
	    })
	
	var oo = node.selectAll('g .node');
	    
	    oo.append('text')
		.attr("dy", ".3em")
		.text("toto");
	    
	    
	    simulation.nodes(data_coin)
    	    	.on("tick", ticked)
	    
	    simulation.force("link")
    	    	.links(links)
	    
	    function ticked() {
		link.attr("x1", function(d) {return d.source.x; })
		    .attr("y1", function(d) {return d.source.y; })
		    .attr("x2", function(d) {return d.target.x; })
		    .attr("y2", function(d) {return d.target.y; });
		
		node.attr("x", function(d) {return d.x;})
		    .attr("y", function(d) {return d.y;});
	    }
	}


    
    var results = function(){
	// effacer le formulaire
	var t0 = fl_grid.transition()
	    .duration(1000)
	    .style('opacity', '0');

	t0.transition()
	    .style('display', 'none');

	data_coin_last.sort(function(a, b){return d3.descending(a.last, b.last);});

	data_coin.forEach(function(d){
	    map_data[d.iso_a3]['d']['tot'] += d.last;
	});

	data_coin.sort(function(a, b){return d3.descending(a.tot, b.tot);});

	d3.select('#vp-infos')
	    .transition()
	    .duration(1000)
	    .style('top', '-1080px');
	
	// afficher le bouton suivant
	d3.select('#vp-infos')
	    .append('div')
	    .attr('class', 'vp-next vp-button')
	//    	    .on('click', next_step)
	    .on('click', function(){
		console.log('next_step');
		next_step();
	    })	  
	    .append('div')
	    .attr('class', 'vp-next-text')    
	    .text('Suivant');
	
	
	var x = d3.scaleLinear().range([0, bar_w]);
	x.domain([0, d3.max(data_coin, function(d) {return d.last})]);
	
	var y = d3.scaleBand().range([0, bar_h], 0.15).padding(0.15);
	y.domain(data_coin_last.map(function(d) { return d.iso_a3}));
	
	var colorScale = d3.scaleLinear()
	    .domain([0, max_coin])
	    .range([color_min, color_max]);
		 
	// var
	//     yAxis = d3.svg.axis()
	// 	.scale(y)
	// 	.orient("left");
	// //	.tickFormat(d3.time.format("%Y-%m"));

	diag_links.selectAll('line')
	    .data(data_coin)
	    .enter()
	    .append('line')
	    .style("stroke-width", "0")
	    .style("stroke", "#000")
	    .style("opacity", "0")
	    .attr("x1", function(d){return FRA_pts[0]})
	    .attr("y1", function(d){return FRA_pts[1]})
	    .attr("x2", function(d){
		return d['centroid'][0];})
	    .attr("y2", function(d){return d['centroid'][1]});
	
	//        .call(yAxis);     
	
	var bars = svg.append("g")
	    .attr("transform", "translate(200, 100)");
	var flags;
	
	var bars_last = svg.append("g")
	    .attr("transform", "translate(200, 100)");
	
	bars_last.selectAll("rect")
	    .data(data_coin)
	    .enter()
	    .append('rect')
	    .style('fill', '#003399')
	    .style('opacity', '0.7')
	    .style("stroke", '#111')
	    .attr("y", function(d, i){return y(d.iso_a3);})
    	    .attr("width", 0)
	    .attr("x", 0) //function(d){return bar_h - x(d.last)})
	    .attr("height", y.bandwidth)
	    .transition()
	    .duration(1500)
            .attr("width", function(d){return x(d.last)});
	
	bars.selectAll("rect")
	    .data(data_coin)
	    .enter()
	    .append('rect')
	    .style('fill', '#ffcc00')
	    .style('opacity', '0.7')
	    .style("stroke", '#111')
	    .attr("y", function(d, i){return y(d.iso_a3);})
    	    .attr("width", 0)
	    .attr("x", 0) //function(d){return bar_h - x(d.last)})
	    .attr("height", y.bandwidth)
	    .transition()
	    .duration(1500)
            .attr("width", 0);
	
	flags_c.transition()
	    .duration(1500)
	    .attr("transform", "translate(200, 100)");

	flags_c.selectAll("g .ticks")
	    .transition()
	    .duration(1500)
   	    .attr("transform", function(d){
		var hh = (y.bandwidth()/2) * d['prop'] + 55;
		return "translate(-" + hh + ", " + y(d.iso_a3) + ")"
	    })
	    .select('image')
    	    .attr("height", y.bandwidth())
    	    .attr("width", 100);

	var xAxis = d3.axisTop()
	    .scale(x);

	svg.append("g")
	    .attr("class", "xAxis")
	    .attr("transform", "translate(200, 100)")
	    .call(xAxis)
	    .append("text")
	    .attr('class', 'xAxis-title')
	    .attr("transform", "translate("+bar_w/2+", -35)")
	    .text("Nombre de pièces dans vos poches");
	
	histo_1 = function(){
	    
	    data_coin.sort(function(a, b){return d3.descending(a.last, b.last);});

	    d3.select(".xAxis-title")
		.transition()
		.duration(1000)
		.text("Nombre de pièces dans vos poches");
	    
	    
	    diag_links.selectAll('line')
		.transition()
		.duration(1500)
		.style("opacity", 0)
		.style("stroke-width", 0);
	    
	    data_coin.sort(function(a, b){return d3.descending(a.last, b.last);});
	    
//	    var x = d3.scaleLinear().range([0, bar_w]);
	    x.domain([0, d3.max(data_coin, function(d) {return d.last})]);
	    
//	    var y = d3.scaleBand().range([0, bar_h], 0.15).padding(0.15);
	    y.domain(data_coin.map(function(d) { return d.iso_a3}));

	    d3.select('.xAxis')
		.transition()
		.duration(1500)
		.style("opacity", "1")
		.call(xAxis);
	    
	    flags_c.transition()
		.duration(1500)
		.attr("transform", "translate(200, 100)");
	    
	    bars.transition()
		.duration(1500)
		.attr("transform", "translate(200,100)");
	    
	    console.log('click');
	    data_coin.sort(function(a, b){return d3.descending(a.last, b.last);});
	    
	    bars_last.selectAll('rect')
		.transition()
		.duration(1500)
		.attr("width", function(d){return x(d.last)})
		.attr("height", y.bandwidth())
		.attr("y", function(d, i){return y(d.iso_a3);})
	    	.attr("x", 0);

	    bars.selectAll('rect')
		.attr("width", 0)
		.transition()
		.duration(1500)
		.attr("width", 0)
		.attr("height", y.bandwidth())
		.attr("y", function(d, i){return y(d.iso_a3);})
	    	.attr("x", 0);
	    
	    
	    flags_c.selectAll('g .ticks')
		.transition()
		.duration(1500)
		.attr("transform", function(d){
		    var hh = (y.bandwidth()/2)*d['prop']+55;
		    //return "translate(0, 0)";
		    return "translate(-" + hh + ", " + y(d.iso_a3) + ")"
		})
		.select('image')
    		.attr("height", y.bandwidth())
    		.attr("width", 100);

	};

	histo_2 = function(){

	    simulation.stop();

	    d3.select(".xAxis-title")
		.transition()
		.duration(1000)
		.text("Nombre de pièces les poches des autres participants");
	
	    console.log('click');
	    data_coin.sort(function(a, b){return d3.descending(a.tot, b.tot);});
	    
//	    x_2 = d3.scaleLinear().range([0, bar_w]);
	    x.domain([0, d3.max(data_coin, function(d) {return d.tot})]);
	    
//	    y = d3.scaleBand().range([0, bar_h]);
	    y.domain(data_coin.map(function(d) { return d.iso_a3}));

	    d3.select('.xAxis')
		.transition()
		.duration(2000)
		.call(xAxis);
	    
	    var t0 = bars_last.selectAll('rect')
		.transition()
		.duration(2500)
		.attr("width", function(d){return x(d.last)});
	//	.attr("y", function(d, i){return y(d.iso_a3);});

	    t0.transition()
		.duration(1000)
	    	.attr("y", function(d, i){return y(d.iso_a3);});
	    
	    var t1 = bars.selectAll('rect')
		.transition()
		.duration(2500)
		.attr("width", function(d){return x(d.tot)});

	    t1.transition()
		.duration(1000)
		.attr("y", function(d, i){return y(d.iso_a3);});
	    
	    flags_c.selectAll('g .ticks')
		.transition()
		.delay(2500)
		.duration(1000)
		.attr("transform", function(d){
		    var hh = (y.bandwidth()/2)*d['prop']+55;
		    //return "translate(0, 0)";
		    return "translate(-" + hh + ", " + y(d.iso_a3) + ")"
		});	
	}

	histo_map = function(){
	    console.log('click')


	    x_3 = d3.scaleLinear().range([0, 300]);
	    x_3.domain([0, d3.max(data_coin, function(d) {return d.tot})]);

	    d3.select('.xAxis')
		.transition()
		.duration(1500)
		.style("opacity", 0);
	    
	    flags_c.transition()
		.duration(1500)
		.attr("transform", "translate(0, 0)");

	    bars.transition()
		.duration(1500)
		.attr("transform", "translate(0,0)");

	    bars_last.selectAll('rect')
		.transition()
		.duration(1500)
		.style("opacity",0.7)
		.attr("width", 0)
		.attr("height", 0)
		.attr("x", function(d){return d.centroid[0]-10})
		.attr("y", function(d, i){
		    return d.centroid[1] - 10;
		});
	    	    
	    bars.selectAll('rect')
		.transition()
		.duration(1500)
		.style("opacity",0.7)
		.attr("width", 20)
		.attr("height", function(d){return x_3(d.tot)})
		.attr("x", function(d){return d.centroid[0]-10})
		.attr("y", function(d, i){
		    return d.centroid[1] - x_3(d.tot) - 10;
		});
	    
	    
	    flags_c.selectAll('g .ticks')
		.transition()
		.duration(1500)
		.attr('transform', function(d){
		    var xx = d['centroid'][0] - 25;
		    var yy = d['centroid'][1] - 10;
		    return 'translate('+xx+', '+yy+')'})
		.select('image')
    		.attr("height", 20)
		.attr("width", 50);
	    use_color_scale = false;
	    if (use_color_scale){
		d3.select('#regions').selectAll('path')
		    .transition()
		    .duration(750)
		    .style("opacity", 1)
		    .style("fill", function(d){
			if (lst_iso.indexOf(d.properties.iso_a3)>=0){
			    return colorScale(map_data[d.properties.iso_a3]['tot']);
			}else{
			    return "#999";
			}
		    })
		    .style("stroke", "#000");
		//		.attr("d", path);
	    }else{
		d3.select('#regions').selectAll('path')
		    .transition()
		    .duration(750)
		    .style("opacity", 1)
		    .style("fill", function(d){
			if (lst_iso.indexOf(d.properties.iso_a3)>=0){
			    return "#fff";
			}else{
			    return "#999";
			}
		    })
		    .style("stroke", "#000");
	    }
	}

	links_diagram = function(){

	    w = d3.scaleLinear().range([0, 40]);
	    w.domain([0, max_coin_fr]);
	    
	    bars.selectAll('rect')
		.transition()
		.duration(1000)
		.attr("y", function(d, i){
		    return d.centroid[1] - 10;})
		.attr("height", 0);
	    
	    var tt = diag_links.selectAll('line')
		.transition()
		.duration(1500)
		.style("opacity", 0.7)
		.style("stroke", "#ffcc00") 
		.style("stroke-width", function(d){
		    return w(d.tot);
		});
	}
	

	sim_diag = function(){

	    
	    d3.select('.vp-next')
		.transition()
		.duration(750)
		.style('top', '1750px');

	    // afficher le bouton a propos
	    var apropos_stat = d3.select('#vp-infos')
		.append('div')
		.attr('class', 'vp-button vp-apropos-statistics')
		.on('click', function(){
		    console.log("http://" + window.location.host + '/apropos');
		    window.location.href = "http://" + window.location.host + '/apropos';
		})	  
		.text('En savoir plus')
		.style('opacity', '0');
	

	    apropos_stat.transition()
		.delay(750)
		.duration(750)
		.style('opacity', '1');
		       
	    flags_c.transition()
		.duration(1500)
		.attr("transform", "translate(0, 0)");

	    bars.transition()
		.duration(1500)
		.attr("transform", "translate(0,0)");
	    
	    diag_links.selectAll('line')
		.transition()
		.duration(1500)
		.style("opacity", 0)
		.style("stroke-width", 0);

	    d3.select('#regions').selectAll('path')
		.transition()
		.duration(1500)
		.style("opacity", "0.4")
		.style("fill", function(d){
		    if (data_coin.find(function(j){return j.iso_a3 == d.properties.iso_a3})){
			return "#fff";
		    }else{
			return "#999";
		    }
		});
	    
	    flags_c.selectAll('g .ticks')
		.transition()
		.duration(1500)
		.attr('transform', function(d){
		    var xx = d.x - d.ww/2;
		    var yy = d.y - d.hh/2;
		    return 'translate('+xx+', '+yy+')'})
		.select('image')
    		.attr("height", function(d){
		    return d.hh;
		})
		.attr("width", function(d){
		    return d.ww;
		});
	}

	
	
	lst_step = [histo_1,
		    histo_2,
		    histo_map,
		    links_diagram,
		    sim_diag]

	next_step = function(){
	    console.log('next step');
	    lst_step[i_step]();
	    i_step = (i_step+1) % lst_step.length;
	}
    }
})


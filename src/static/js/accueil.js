var data_coin;
var regions;
var path;
var carto;
var features;
var map_data;

$(document).ready(function(){
    
    var width = 1920;
    var height = 1075;

    var svg = d3.select('#container')
	.append("svg")
	.attr("width", width)
    	.attr("height", height);

    
    var draw = function(error, data){
	console.log(data);
	if (error) return console.error(error);
	
	var topology = data;
	var geometries = topology.objects.collection;
	
	projection = d3.geoMercator()
 	    .scale(1100)
 	    .translate([500, 1650]);
	
	path = d3.geoPath()
	    .projection(projection);
	
	regions = svg.append('g')
	    .attr("id", "regions");
//	    .selectAll("path");
	
	carto = d3.geoPath()
	    .projection(projection);
	
	features = topojson.feature(topology,geometries).features;

	regions = regions.selectAll('path')
	    .data(features)
	    .enter()
	    .append("path")
	    .attr("class", "region")
	    .style("opacity", "0.5")
	    .attr("d", path);
	
	regions.data(features)
	    .transition()
	    .duration(750)
	    .style("fill", "#fff")
	    .style("stroke", "#000");
    }

    d3.json("static/data/europe.topojson", draw);


    if ($('#toapropos').length){
	    d3.select('#vp-accueil')
		.style('top', '-100%');
	}
    
    d3.selectAll('.vp-jouer')
	.on('click', function(){
	    window.location.href = 'http://' + window.location.host + '/results';
	});

    d3.select('.vp-apropos')
	.on('click', function(){
	    console.log('a propos click');
	    d3.select('#vp-accueil')
		.transition()
		.duration(1000)
		.style('top', '-100%');
	});

    d3.select('.vp-apropos-accueil')
	.on('click', function(){
	    console.log('a propos click');
	    d3.select('#vp-accueil')
		.transition()
		.duration(1000)
		.style('top', '0%');
	});
})

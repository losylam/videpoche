Installation 
==


Sans virtual env
-- 

    sudo apt-get install python-pip

    git clone https://gitlab.com/losylam/videpoche.git
    cd videpoche
    sudo pip install -r requirements.txt

    cd src
    python server.py


Avec virtual-env
-- 

    sudo apt-get install python-pip

    git clone https://gitlab.com/losylam/videpoche.git
    cd videpoche

    sudo pip install virtualenv

    virtualenv -p python2.7 venv
    source venv/bin/activate
    pip install -r requirements.txt

    cd src
    python server.py
	
Lancement de l'appli
==

    cd videpoche/src
	python server.py
	
pour visiter le site
    
	firefox localhost:5000
